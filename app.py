from flask import Flask, jsonify, render_template
import json

app = Flask(__name__)


@app.route('/')
def index():
 
    return render_template('demo.html', greeting="hello", )

@app.route('/sample')
def sample():

    return render_template('demo_1.html', greeting="hello_1")

@app.route('/json_data')
def json_data():
    data = {'msg': 'hello'}
    data = json.dumps(data)
    return data

if __name__ == '__main__':
    app.run(debug=True)
    