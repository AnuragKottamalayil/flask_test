import json


# To test json response
def test_json_data(app, client):
    res = client.get('/json_data')
    assert res.status_code == 200
    expected = {'msg': 'hello'}
    assert expected == json.loads(res.get_data(as_text=True))
    
    
# To test templates
def test_my_view_(client, captured_templates):
    response = client.get("/")
    assert len(captured_templates) == 1
    template, context = captured_templates[0]
    assert template.name == "demo.html"
    assert "greeting" in context
    assert context["greeting"] == "hello"

def test_my_view_1(client, captured_templates):
    response = client.get("/sample")
    assert len(captured_templates) == 1
    template, context = captured_templates[0]
    assert template.name == "demo_1.html"
    assert "greeting" in context
    assert context["greeting"] == "hello_1"
    
